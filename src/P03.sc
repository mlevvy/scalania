object P03 {
  def nth3(k: Int, l:List[Int]):Int = (k,l) match {
    case (0,h ::t) => h
    case(n, h :: t) => nth3(n-1,t)
    case e => throw new Exception
  }

  def nth4(k: Int, l:List[Int]):Int = k match {
    case 0 => l.head
    case _ => nth4(k-1,l.tail)
  }
  nth3(1,List(1,2,3,4))
  nth4(1,List(1,2,3,4))

}

