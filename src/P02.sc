import scala.annotation.tailrec

object P02 {
  def oneLast(ls: List[Int]):Int = ls match {
    case Nil  => throw new Exception
    case head :: _ :: Nil => head
    case _ :: tail   => oneLast(tail)
  }

  @tailrec
  def oneLast2(ls: List[Int]):Int = ls match {
    case h :: t   => t match{
      case e :: Nil => h
      case u :: us => oneLast2(t)
    }
    case _ => throw new Exception
  }
  def oneLast3(ls: List[Int]):Int = ls match {
    case a :: b :: Nil => a
    case h :: t => {
      println(h,t)
      oneLast3(t)
    }
    case Nil => throw new Exception
  }
//  def oneLast4(ls: List[Int]):Int = ls reverse 1
  def oneLast4(ls: List[Int]):Int = ls.reverse.apply(1)
  //Konwencja: Jezeli metoda jest bez nawiasow, to nie ma efektow ubocznych

  oneLast(List(0,1,2,3,4))
  oneLast2(List(0,1,2,3,4))
  oneLast3(List(0,1,2,3,4))
  oneLast3(List(0,1))
  oneLast4(List(0,1,2,3,4))
}




