object P01 {
  def lastRecursive(ls: List[Int]):Int = ls match {
    case Nil  => throw new Exception
    case head :: Nil => head
    case _ :: tail   => lastRecursive(tail)
  }

  lastRecursive(List(5,4,3,2))
}
