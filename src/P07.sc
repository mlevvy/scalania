object P07 {
  def flatten1(ls: List[Any]): List[Any] = ls flatMap {
    case ms: List[_] => flatten1(ms)
    case e => List(e)
  }
  def flatten2(in: List[Any]): List[Any] = in match{
    case (head:List[Any]) :: tail => flatten2(head)++flatten2(tail)
    case head::tail => head :: flatten2(tail)
    case _ => Nil
  }
  flatten1(List(List(1, 1), 2, List(3, List(5, 8))))
  flatten2(List(List(1, 1), 2, List(3, List(5, 8))))

}


