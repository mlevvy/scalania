object P05 {
  def reverse1(l:List[Int]):List[Int] = l.reverse
  def reverse2(l:List[Int]):List[Int] = {
    def myReverse(original:List[Int], acc: List[Int]):List[Int]= original match{
      case Nil => acc
      case h ::t =>myReverse(t,h::acc)
    }
    myReverse(l,Nil)
  }
  def reverse3(l:List[Int]):List[Int] = {
    var lst = l
    var reversed:List[Int]= Nil
    while(lst.nonEmpty) {
      reversed ::= lst.head
      lst = lst.tail
    }
    reversed
  }
  def reverse4(l:List[Int]):List[Int] =  (List[Int]() /: l ) {(h,r) => r :: h}
  def reverse5(l:List[Int]):List[Int] =  (l.foldLeft(List[Int]())) {(h,r) => r :: h}
  def reverse6(l:List[Int]):List[Int] =  (l./:(List[Int]())) {(h,r) => r :: h}
  reverse1(List(1,2,3,4))
  reverse2(List(1,2,3,4))
  reverse3(List(1,2,3,4))
  reverse4(List(1,2,3,4))
  reverse5(List(1,2,3,4))
}




