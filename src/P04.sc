import scala.annotation.tailrec

object P04 {
  def myLength(l:List[Int]):Int = {
    def len(l:List[Int], s:Int):Int = l match{
      case h :: t => len(t, s+1)
      case _ => s
    }
    len(l,0)
  }

  //@tailrec //to nie jest ogonowe.
  def myLength2(l:List[Int]):Int = l match{
    case h :: t => myLength2(t) + 1
    case Nil => 0
  }
  myLength(List(1,2,3,4))
  myLength2(List(1,2,3,4))
}


