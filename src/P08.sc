object P08 {
  def compress(l: List[Symbol]): List[Symbol] =
    l.foldLeft(List[Symbol]()){(ls,e) =>
      if (ls.nonEmpty && ls.head == e) ls
      else e::ls
    }

  def compress2(l: List[Symbol]): List[Symbol] =
    l.foldRight(List[Symbol]()){(e,ls) =>
      if (ls.nonEmpty && ls.head == e) ls
      else e::ls
    }
  compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
  compress2(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
}




